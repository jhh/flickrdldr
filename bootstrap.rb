require 'flickraw'
require 'active_record'
require 'standalone_migrations'

# Require models, and other dependencies in lib folder.
# Classes in common folder are expected to be loaded first due
# to order of dependencies.
Dir['lib/common/**/*.rb', 'lib/**/*.rb'].uniq.each do |file|
  require File.join(Dir.pwd, file)
end

# Establish database connection using implementation from StandaloneMigration.
configurator = StandaloneMigrations::Configurator.new
ActiveRecord::Base.establish_connection configurator.config_for(Rails.env)

# Check if database is configured.
begin
  FlickrDldr::Authentication::AuthData.new.api_key
rescue => e
  puts "Please run \"rake db:setup\" and try again."
  raise e
end

# Include module.
include FlickrDldr
