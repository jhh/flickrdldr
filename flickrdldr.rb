#!/usr/bin/env ruby
require_relative 'bootstrap'
Authentication.login
puts 'Import recent or all photos? Press enter for recent, press "a" then enter for all.'
print '> '
recent_or_all = gets.strip.downcase
if recent_or_all[0] == 'a'
  Import.full
else
  Import.recent
end
