class CreateSearchResultsTable < ActiveRecord::Migration
  def up
    create_table :search_results do |t|
      t.integer :id
      t.string :owner
      t.string :secret
      t.string :server
      t.integer :farm
      t.string :title
      t.integer :ispublic
      t.integer :isfriend
      t.integer :isfamily
      t.string :dateupload
      t.string :originalsecret
      t.string :originalformat
      t.string :url_o
      t.string :height_o
      t.string :width_o
      t.timestamps
    end
  end

  def down
    drop_table :search_results
  end
end
