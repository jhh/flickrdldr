class InitialMigration < ActiveRecord::Migration
  def up
    create_table :configs do |t|
      t.string :key
      t.string :value
      t.timestamps
    end
  end

  def down
    drop_table :configs
  end
end
