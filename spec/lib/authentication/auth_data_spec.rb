require 'spec_helper'
module FlickrDldr
  module Authentication
    describe AuthData do
      subject{AuthData.new(Models::Config)}
      before(:each) {Models::Config.delete_all}
      describe '#api_key' do
        it 'stores API key' do
          subject.api_key = 'foo'
        end
        it 'retrives stored API key' do
          subject.api_key = 'foo'
          expect(subject.api_key).to eq('foo')
        end
      end
      describe '#shared_secret' do
        it 'stores shared secret' do
          subject.shared_secret = 'bar'
        end
        it 'retrives stored secret' do
          subject.shared_secret = 'bar'
          expect(subject.shared_secret).to eq('bar')
        end
      end
      describe '#access_token' do
        it 'stores access token' do
          subject.access_token = 'baz'
        end
        it 'retrives access token' do
          subject.access_token = 'baz'
          expect(subject.access_token).to eq('baz')
        end
      end
      describe '#access_secret' do
        it 'stores access secret' do
          subject.access_secret = 'bat'
        end
        it 'retrives access secret' do
          subject.access_secret = 'bat'
          expect(subject.access_secret).to eq('bat')
        end
      end
      describe '#login_id' do
        it 'stores login id' do
          subject.login_id = 'bam'
        end
        it 'retrives login id' do
          subject.login_id = 'bam'
          expect(subject.login_id).to eq('bam')
        end
      end
      describe '#login_username' do
        it 'stores login username' do
          subject.login_username = 'ban'
        end
        it 'retrives login username' do
          subject.login_username = 'ban'
          expect(subject.login_username).to eq('ban')
        end
      end
    end
  end
end
