require 'spec_helper'
require 'tempfile'
module FlickrDldr
  module Import
    describe ImportData do
      let(:response) {OpenStruct.new({
        "id"=>"17777",
        "owner"=>"72962646@N00",
        "secret"=>"dfd7d7c088",
        "server"=>"2",
        "farm"=>1,
        "title"=>"Foo Bar",
        "ispublic"=>0,
        "isfriend"=>0,
        "isfamily"=>0,
        "dateupload"=>"1101669028",
        "originalsecret"=>"dfd5d5c088",
        "originalformat"=>"jpg",
        "url_o"=>File.join(File.expand_path(File.dirname(__FILE__)), 'import_data_sample.jpg'),
        "height_o"=>"352",
        "width_o"=>"288"
      })}
      subject{ImportData.new(Models::Config, Models::SearchResult)}
      before(:each) {
        Models::Config.delete_all
        Models::SearchResult.delete_all
      }
      describe '#most_recent_upload' do
        it 'stores date upload for most recent imported photo/media' do
          subject.most_recent_upload = 'foo'
        end
        it 'retrives date upload for most recent imported photo/media' do
          subject.most_recent_upload = 'foo'
          expect(subject.most_recent_upload).to eq('foo')
        end
      end
      describe '#store_search_response' do
        it 'stores response' do
          expect(Models::SearchResult.exists?("17777")).to be_falsey
          subject.store_search_response(response)
          expect(Models::SearchResult.exists?("17777")).to be_truthy
        end
        it 'updates last date upload' do
          expect(subject.most_recent_upload).to eq(nil)
          subject.store_search_response(response)
          expect(subject.most_recent_upload).to eq("1101669028")
        end
        it 'updates existing response' do
          sr = Models::SearchResult.new
          sr.id = 17777
          sr.save
          expect(Models::SearchResult.find(17777).dateupload).to eq(nil)
          subject.store_search_response(response)
          expect(Models::SearchResult.find(17777).dateupload).to eq("1101669028")
        end
      end
      describe '#get_file_path' do
        it 'returns file path from search response' do
          file_path = subject.get_file_path(response)
          file_path_expected = File.join(Dir.pwd, 'import/2004-11-28/17777_dfd5d5c088.jpg')
          expect(file_path).to eq(file_path_expected)
        end
        it 'returns file path from search result in database' do
          subject.store_search_response(response)
          file_path = subject.get_file_path(Models::SearchResult.find(response.id))
          file_path_expected = File.join(Dir.pwd, 'import/2004-11-28/17777_dfd5d5c088.jpg')
          expect(file_path).to eq(file_path_expected)
        end
      end
      describe '#store_search_file' do
        it 'saves sample file from search response' do
          file = Tempfile.new('foo')
          file.close
          begin
            expect(File.size?(file.path)).to eq nil
            subject.store_search_file(response, file.path)
            expect(File.size?(file.path)).to be > 0
          ensure
            file.close
            file.unlink
          end
        end
        it 'saves sample file from database record' do
          subject.store_search_response(response)
          file = Tempfile.new('bar')
          file.close
          begin
            expect(File.size?(file.path)).to eq nil
            subject.store_search_file(Models::SearchResult.find(response.id), file.path)
            expect(File.size?(file.path)).to be > 0
          ensure
            file.close
            file.unlink
          end
        end
      end
    end
  end
end
