require 'spec_helper'
module FlickrDldr
  module Common
    describe CommonData do
      subject{CommonData.new(Models::Config)}
      before(:each) {Models::Config.delete_all}
      describe '#set_config_value' do
        it 'store value' do
          subject.set_config_value('foo', 'bar')
          expect(subject.get_config_value('foo')).to eq('bar')
        end
        it 'deletes value if given "nil"' do
          subject.set_config_value('foo', 'bar')
          expect(subject.get_config_value('foo')).to eq('bar')
          subject.set_config_value('foo', nil)
          expect(subject.get_config_value('foo')).to eq(nil)
          expect(Models::Config.where(key: 'foo').count).to be == 0
        end
      end
      describe '#get_config_value' do
        it 'returns nil for no value' do
          expect(subject.get_config_value('foo')).to eq(nil)
        end
      end
      describe '#delete_config_value' do
        it 'deletes key' do
          subject.set_config_value('foo', 'bar')
          subject.delete_config_value('foo')
          expect(subject.get_config_value('foo')).to eq(nil)
        end
      end
    end
  end
end
