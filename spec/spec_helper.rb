require 'ostruct'
require File.join(Dir.pwd, 'bootstrap')
# Disconnect connection establised in bootstrap.
ActiveRecord::Base.connection.disconnect!
# Establish a memory database, and load the database schema.
# Clues from
# http://blog.markstarkman.com/blog/2013/01/23/using-sqlite-to-test-active-record-models/
ActiveRecord::Base.establish_connection adapter: "sqlite3", database: ":memory:"
load File.join(Dir.pwd, 'db/schema.rb')
