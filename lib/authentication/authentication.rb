module FlickrDldr
  module Authentication
    def self.configure_api_key
      auth_data = AuthData.new
      puts "Enter API key."
      print "> "
      auth_data.api_key = gets.strip
      auth_data.api_key
    end

    def self.configure_shared_secret
      auth_data = AuthData.new
      puts "Enter shared secret."
      print "> "
      auth_data.shared_secret = gets.strip
      auth_data.shared_secret
    end

    def self.configure_login_token
      auth_data = AuthData.new
      FlickRaw.api_key = auth_data.api_key || self.configure_api_key
      FlickRaw.shared_secret = auth_data.shared_secret || self.configure_shared_secret
      token = flickr.get_request_token
      auth_url = flickr.get_authorize_url(token['oauth_token'], :perms => 'read')
      puts "Open this url in your browser to authorize access: %s" % auth_url
      puts "Enter the verification code from web page to complete the process."
      print "> "
      verify = gets.strip
      begin
        flickr.get_access_token(token['oauth_token'], token['oauth_token_secret'], verify)
        auth_data.access_token = flickr.access_token
        auth_data.access_secret = flickr.access_secret
      rescue FlickRaw::FailedResponse => e
        puts "Configure access token failed."
        puts "%s" % e.msg
        raise e
      end
    end

    def self.login
      auth_data = AuthData.new
      FlickRaw.api_key = auth_data.api_key || self.configure_api_key
      FlickRaw.shared_secret = auth_data.shared_secret || self.configure_shared_secret
      if auth_data.access_token && auth_data.access_secret
        flickr.access_token, flickr.access_secret = auth_data.access_token, auth_data.access_secret
      else
        self.configure_login_token
      end

      # From here you are logged:
      login = flickr.test.login
      auth_data.login_id = login.id
      auth_data.login_username = login.username
      puts "You are now authenticated as %s." % auth_data.login_username
    end

    def self.reset_login_data
      auth_data = AuthData.new
      auth_data.access_token = nil
      auth_data.access_secret = nil
      auth_data.login_id = nil
      auth_data.login_username = nil
    end
  end
end
