module FlickrDldr
  module Authentication
    class AuthData < FlickrDldr::Common::CommonData

      KEY_API_KEY = 'auth_data.api_key'
      KEY_SHARED_SECRET = 'auth_data.shared_secret'
      KEY_ACCESS_TOKEN = 'auth_data.access_token'
      KEY_ACCESS_SECRET = 'auth_data.access_secret'
      KEY_LOGIN_ID = 'auth_data.login_id'
      KEY_LOGIN_USERNAME = 'auth_data.login_username'

      def api_key=(value)
        set_config_value(KEY_API_KEY, value)
      end

      def api_key
        get_config_value(KEY_API_KEY)
      end

      def shared_secret=(value)
        set_config_value(KEY_SHARED_SECRET, value)
      end

      def shared_secret
        get_config_value(KEY_SHARED_SECRET)
      end

      def access_token=(value)
        set_config_value(KEY_ACCESS_TOKEN, value)
      end

      def access_token
        get_config_value(KEY_ACCESS_TOKEN)
      end

      def access_secret=(value)
        set_config_value(KEY_ACCESS_SECRET, value)
      end

      def access_secret
        get_config_value(KEY_ACCESS_SECRET)
      end

      def login_id=(value)
        set_config_value(KEY_LOGIN_ID, value)
      end

      def login_id
        get_config_value(KEY_LOGIN_ID)
      end

      def login_username=(value)
        set_config_value(KEY_LOGIN_USERNAME, value)
      end

      def login_username
        get_config_value(KEY_LOGIN_USERNAME)
      end
    end
  end
end
