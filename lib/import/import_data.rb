require 'open-uri'
require 'fileutils'

module FlickrDldr
  module Import
    class ImportData < FlickrDldr::Common::CommonData

      KEY_MOST_RECENT_UPLOAD = 'import_data.most_recent_upload'
      SEARCH_PER_PAGE = 10

      def initialize(config = FlickrDldr::Models::Config, search_result = FlickrDldr::Models::SearchResult)
        @search_result = search_result
        super(config)
      end

      # Storage for upload date in unix time for recent import photo/media.
      def most_recent_upload=(value)
        set_config_value(KEY_MOST_RECENT_UPLOAD, value)
      end

      def most_recent_upload
        get_config_value(KEY_MOST_RECENT_UPLOAD)
      end

      def store_search_file(object, file_path=nil)
        file_path = file_path || get_file_path(object)
        # http://stackoverflow.com/a/5021245
        unless Dir.exists?(File.dirname(file_path))
          FileUtils.mkdir_p(File.dirname(file_path))
        end
        # http://stackoverflow.com/a/9311926
        File.open(file_path, 'wb') do |local_file|
          begin
            open(object.url_o, 'rb') do |remote_file|
              local_file.write(remote_file.read)
            end
          rescue => e
            puts "Failed to download %s.." % object.url_o
            raise e
          end
        end
      end

      def get_file_path(object)
        filename = "%s_%s.%s" % [object.id, object.originalsecret, object.originalformat]
        dateupload = object.dateupload.to_i
        datestr = Time.at(dateupload).to_date.strftime("%Y-%m-%d")
        File.join(Dir.pwd, 'import', datestr, filename)
      end

      # Stores search response and updates most_recent_upload.
      def store_search_response(response)
        item = if @search_result.exists?(response.id)
                 @search_result.find(response.id)
               else
                 @search_result.new
               end
        item.id = response.id
        item.owner = response.owner
        item.secret = response.secret
        item.server = response.server
        item.farm = response.farm
        item.title = response.title
        item.ispublic = response.ispublic
        item.isfriend = response.isfriend
        item.isfamily = response.isfamily
        item.dateupload = response.dateupload
        item.originalsecret = response.originalsecret
        item.originalformat = response.originalformat
        item.url_o = response.url_o
        item.height_o = response.height_o
        item.width_o = response.width_o
        item.save
        if item.dateupload.to_i > self.most_recent_upload.to_i
          self.most_recent_upload = item.dateupload
        end
      end

      # Find photos/media sorted by upload date, old->new.
      # min_upload_date: start date in unix timestamp.
      # page: start page, from 1 and up.
      def self.find_photos(min_upload_date=0, page=1)
        flickr.photos.search(
          user_id: 'me',
          sort: 'date-posted-asc',
          page: page,
          extras: 'url_o,original_format,date_upload',
          per_page: SEARCH_PER_PAGE,
          min_upload_date: min_upload_date
        )
      end

      def self.import(recent_only=true)
        import_data = ImportData.new
        most_recent_upload = recent_only ? import_data.most_recent_upload.to_i : 0
        page = 0
        loop do
          page += 1 # Note: Page is always >= 1.
          search_result = self.find_photos(most_recent_upload, page)
          break if search_result.count == 0
          search_result.each do |response|
            import_data.store_search_file(response)
            import_data.store_search_response(response)
            yield response if block_given?
          end
        end
      end
    end
  end
end
