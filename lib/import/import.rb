module FlickrDldr
  module Import
    def self.recent
      self._import(true)
    end

    def self.full
      self._import(false)
    end

    def self._import(recent_only=true)
      import_data = Import::ImportData.new
      Import::ImportData.import(recent_only) do |r|
        path = import_data.get_file_path(r).sub(Dir.pwd, '.')
        puts "%s saved into %s.." % [r.title, path]
      end
    end
  end
end
