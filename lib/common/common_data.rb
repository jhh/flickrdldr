require File.join(Dir.pwd, 'lib/models/config')
module FlickrDldr
  module Common
    class CommonData
      # Expect ActiveRecord Config object.
      def initialize(config = FlickrDldr::Models::Config)
        @config = config
      end

      def set_config_value(key, value)
        if value.nil?
          delete_config_value(key)
        else
          items = @config.where(key: key)
          if items.empty?
            Models::Config.create(key: key, value: value)
          elsif items.count > 1
            raise "No unique key for %s" % key
          else
            items[0].value = value
            items[0].save
          end
        end
      end

      def get_config_value(key)
        result = nil
        items = @config.where(key: key)
        if items.count > 1
          raise "No unique key for %s" % key
        elsif items.count == 1
          result = items[0].value
        end
        result
      end

      def delete_config_value(key)
        @config.delete_all(['key = ?', key])
      end
    end
  end
end
